from dataflows import Flow, load, dump_to_path, dump_to_zip, printer, add_metadata
from dataflows import sort_rows, filter_rows, find_replace, delete_fields, set_type, validate, unpivot


def gold_spot_prices_daily_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/gold-spot-prices-daily.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='gold_spot_prices_daily_csv', title='''gold-spot-prices-daily.csv'''),
        printer(),
        dump_to_path('gold_spot_prices_daily_csv'),
    )
    flow.process()

def unemployment_rate_monthly_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/unemployment-rate-monthly.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='unemployment_rate_monthly_csv', title='''unemployment-rate-monthly.csv'''),
        printer(),
        dump_to_path('unemployment_rate_monthly_csv'),
    )
    flow.process()

def gdp_quarterly_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/gdp-quarterly.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='gdp_quarterly_csv', title='''gdp-quarterly.csv'''),
        printer(),
        dump_to_path('gdp_quarterly_csv'),
    )
    flow.process()

def federal_funds_rate_daily_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/federal-funds-rate-daily.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='federal_funds_rate_daily_csv', title='''federal-funds-rate-daily.csv'''),
        printer(),
        dump_to_path('federal_funds_rate_daily_csv'),
    )
    flow.process()

def two_year_treasury_daily_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/two-year-treasury-daily.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='two_year_treasury_daily_csv', title='''two-year-treasury-daily.csv'''),
        printer(),
        dump_to_path('two_year_treasury_daily_csv'),
    )
    flow.process()

def ten_year_treasury_daily_csv():
    flow = Flow(
        # Load inputs
        load('https://gitlab.com/monikappv/gold-spot-prices/raw/master/data/ten-year-treasury-daily.csv', format='csv', ),
        # Process them (if necessary)
        # Save the results
        add_metadata(name='ten_year_treasury_daily_csv', title='''ten-year-treasury-daily.csv'''),
        printer(),
        dump_to_path('ten_year_treasury_daily_csv'),
    )
    flow.process()


if __name__ == '__main__':
	gold_spot_prices_daily_csv(),
	unemployment_rate_monthly_csv(),
	gold_spot_prices_daily_csv(),
	federal_funds_rate_daily_csv(),
	two_year_treasury_daily_csv(),
	ten_year_treasury_daily_csv()

This dataset contains time-series U.S. data of:
* Gold spot prices daily (01 Apr 1968 - 06 Dec 2019)
* Unemployment rate monthly (01 Jan 1948 - 01 Nov 2019)
* GDP quarterly (01 Jan 1947 - 01 July 2019)
* Federal funds rate daily (01 July 1954 - 05 Dec 2019)
* 2-year treasury daily (01 June 1976 - 05 Dec 2019)
* 10-year treasury daily (01 Feb 1962 - 05 Dec 2019)

## Data

The data are obtained from the [Federal Reserve Bank of St. Louis](https://fred.stlouisfed.org/).

If the value is ".", it means there are no data for this time period.

## Preparation

Inside the "script" directory, there is a file called process.py which will generate 6 csv files.

## Instructions:

1. Download "requirements.txt".
2. Go to your terminal and use the following command 

`pip install -r requirements.txt`

3. Run the process.py script
4. 6 CSV files will be created in the same directory where you have saved `process.py` file

## License
This Data Package is made available under the Public Domain Dedication and License v1.0
